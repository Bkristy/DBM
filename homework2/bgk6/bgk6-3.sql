/* 
Bruce Kristy bgk6@zips.uakron.edu
Database Managment class 2 fall 2017

homework 2 problem 3

data types found at
 https://www.tutorialspoint.com/sql/sql-data-types.htm
 */

--//<>// exercise 6.1.3 //<>// 
--problem 3 part 1 a
--find model, speed, hd for all pc price < 1000
SELECT model, speed, hd 
from PC
WHERE price < 1000;

--problem 3 part 1 b
--find model, speed, hd for all pc price < 1000
--rename speed->gigahertz, rename hd->gigabytes
SELECT model, speed as gigahertz, hd as gigabytes
from PC
WHERE price < 1000;

--problem 3 part 1 c
--find the manufacturers of printers
select maker
from Product
where type = 'Printer';

--problem 3 part 1 d
--find the model number, memory size, and screen size of laptops > $1500
select model, ram, screen
from Laptop
where price > 1500;

--problem 3 part 1 e
--find all tuples in printer relation for color printers (color is bool)
select *
from Printer
where color = 1;

--problem 3 part 1 f
--find model number and hd size of pc's with price less than $2000
select model, hd
from PC
where price < 2000;

--//<>//  exercise 6.2.2 //<>// 
--problem 3 part 2 a
--give manf, speed of laptops with hd at least 30gb
select Product.maker, Laptop.speed
from Product JOIN Laptop
ON Product.model = Laptop.model
where Laptop.price < 2000;


--problem 3 part 2 b
--find model number and and price of all products made by maker 'B'
select Product.model, price 
from Product, PC
where Product.model = PC.model and maker = 'B'
	union
		select Product.model, price from Product, Laptop
		where Product.model = Laptop.model and maker = 'B'
			union
				select Product.model, price from Product, Printer
				where Product.model = Printer.model and maker = 'B';

--problem 3 part 2 c
--find makers that sell laptops but not pc's
SELECT DISTINCT maker
FROM Product
WHERE type = 'Laptop'
EXCEPT
SELECT DISTINCT maker
FROM Product
WHERE type = 'PC';

--problem 3 part 2 d
--find those hd sizes that occur in two or more pc's
SELECT hd
FROM PC
GROUP BY hd
HAVING COUNT(hd) > 1;

--problem 3 part 2 e
--find pair of pc model have same ram and speed
SELECT PC1.model, PC2.model
FROM PC PC1, PC PC2
where PC1.speed = PC2.speed
AND PC1.ram = PC2.ram
AND pc1.model < PC2.model;

--problem 3 part 2 f
--find maker of 2+ pc or laptop with speed >= 3
SELECT maker
FROM product
where model IN (SELECT model
				FROM PC
				WHERE speed >= 3)
OR model IN (SELECT model
			 FROM Laptop
			 WHERE speed >= 3)
GROUP BY maker
HAVING COUNT(model) > 1;

--//<>//  exercise 6.3.1 //<>// 
--problem 3 part 3 a
--find makers of pc with speed >=3
SELECT maker
FROM product
WHERE model IN (SELECT model
				FROM PC
				WHERE speed >= 3);
				
--problem 3 part 3 b
--find printers with highest price
select model 
from printer 
where price >= all(select price 
				   from printer);

--problem 3 part 3 c
--find laptop speed slower than any pc
--dumb since example pc slowest is lower than laptop
select model
from laptop
where speed <= all(select speed
				  from pc);

--problem 3 part 3 d
--find model number of highest priced item
SELECT model
FROM ((SELECT model, price FROM pc) 
      UNION 
	  (SELECT model, price FROM laptop) 
	   UNION 
	  (SELECT model, price FROM printer)) 
AS models_prices 
WHERE price >= ALL((SELECT price FROM pc) 
	                UNION 
	               (SELECT price FROM laptop) 
	                UNION 
				   (SELECT price FROM printer));

--problem 3 part 3 e
--find maker of color printer with lowest price
select distinct maker
from Product, Printer
where color = 1 and printer.model = product.model
and price <= all(select price from printer where color = 1);

--problem 3 part 3 f
--find maker of pc with fastest processor of pc with smallest ram
select distinct maker from product, pc
where product.model = PC.model
and ram <= all(select ram from PC)
and speed >= all(select speed from pc
				 where ram = (select min(ram) 
							  from PC)
	             );

--//<>//  exercise 6.4.6 //<>// 
--problem 3 part 4 a
--find avg speed of pc's 
select avg(speed)
from PC

--problem 3 part 4 b
--find avg speed of laptops costing over 1000
select avg(speed)
from Laptop
where price > 1000;

--problem 3 part 4 c
--find agv price of pc made by manufacturer A
SELECT AVG(speed) AS avg_speed 
FROM PC
WHERE speed IN (SELECT speed 
                FROM PC, Product 
                WHERE product.model = PC.model 
				AND  maker='A');

--problem 3 part 4 d
--find for each speed, the avg price of a pc
SELECT speed, avg(price) as avg_price
from PC
group by speed;
