/* 
Bruce Kristy bgk6@zips.uakron.edu
Database Managment class 2 fall 2017

homework 2 problem 1

data types found at
 https://www.tutorialspoint.com/sql/sql-data-types.htm
 */
 
Create Table Product (
	maker varchar(max) not NULL,
	--model could have been int based on the example
	--but most models have letter number combinations
	model int unique not NULL,
	--type was flagged by my editor as a keyword?
	type varchar(max) not NULL,
	
	primary key(model)

);

Create Table PC (
	model int unique not NULL,
	--future products may be missing information
	speed float,
	ram int,
	hd int,
	price money,
	
	primary key(model)

);

Create Table Laptop (
	model int unique not Null,
	--future products may be missing information
	speed float,
	ram int,
	hd int,
	screen float,
	price money,
	
	primary key(model)

);

Create Table Printer (
	model int unique not Null,
	--future products may be missing information
	color bit,
	type varchar(max),
	price money,
	
	primary key(model)
);